import React    from "react";
import template from "./About.jsx";

class About extends React.Component {
  constructor(){
    super();
    this.abouts=['About our CEO','About Our Journey','About our Presence','About our Mission','About our Projects','About our giveback','About our roadmap'];
  }
  render() {
    return template.call(this);
  }
}

export default About;
