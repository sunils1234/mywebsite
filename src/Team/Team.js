import React    from "react";
import template from "./Team.jsx";

class Team extends React.Component {
  constructor(){
    super();
    this.teams=['Development Team','QA Team','Support Team','Services Team','Cloud Team','Infrastructure Team','Networking Team'];
  }
  render() {
    return template.call(this);
  }
}

export default Team;
