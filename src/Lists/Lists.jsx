import "./Lists.css";
import React from "react";

function template() {
  let roleNsal=[
    {
      role:'Developer',
      avgSal:25000000
    },
    {
      role:'Lead',
      avgSal:35000000
    },
    {
      role:'Manager',
      avgSal:55000000
    },
    {
      role:'QA',
      avgSal:20000000
    }
  ];
  const questionares=[
     {
      que:"How was the user exp?",
      opt1:"Very good",
      opt2:"good",
      opt3:"average",
      opt4:"needs improvement"
     },
     {
    que:"How was navigating content exp?",
    opt1:"very easy",
    opt2:"easy",
    opt3:"ok",
    opt4:"was not easy at all"
   }
  ];
  return (
    <div className="lists">
      <h1>List of Technologies used in the Product</h1>
      <select>
        {this.technologies.map((x,y)=>{
            return <option key={y}>{x}</option>
        })
        }
      </select>
      <h1> List of Roles in the Product</h1>
      <ol>
        {
        this.state.designations.map((v,i)=>{
           return <li key={i}>{v}</li>
        })
        }
      </ol>
      <h1>Questionares</h1>
      {
        questionares.map((obj,i)=>{
          return <div><h3 key={i}>Q{i+1}.{obj.que}</h3>
                    <div><input type="radio" name={i}/>{obj.opt1}</div>
                    <div><input type="radio" name={i}/>{obj.opt2}</div>
                    <div><input type="radio" name={i}/>{obj.opt3}</div>
                    <div><input type="radio" name={i}/>{obj.opt4}</div>
          </div>
                 
        })
      }
      <br/> 
      <h1>Role And Average Salary</h1>
      <table border="2px">
         <thead>
           <tr>
              <th>Role</th>
              <th>Avg Salary</th>
          </tr>
        </thead>
        <tbody>
         {
           roleNsal.map((obj,i)=>{
             return <tr>
                       <td>{obj.role}</td>
                       <td>{obj.avgSal}</td>
                    </tr>;
           })
         }
        </tbody>
      </table>


      <br/>
      <br/>
      <h1>Complete Information</h1>
    </div>
  );
};

export default template;
