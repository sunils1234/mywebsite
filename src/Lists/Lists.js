import React    from "react";
import template from "./Lists.jsx";

class Lists extends React.Component {
  constructor(){
    super();
    this.technologies=['HTML','CSS','JAVA SCRIPT','BABEL','REACT JS','Amber JS','Backbone JS'];
    this.state={
      designations:['VP','Director','Program Manager','Project Manager','QA Manager','Lead','Developer','QA']
    }
 
  }
  render() {
    return template.call(this);
  }
}

export default Lists;
