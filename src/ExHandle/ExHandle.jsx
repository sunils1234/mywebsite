import "./ExHandle.css";
import React from "react";
import Technology from "./Technology/index";
import ExceptionInterceptor from "./ExceptionInterceptor/index";

function template() {
  return (
    <div className="ex-handle">
      <h1>ExHandle</h1>
      <ExceptionInterceptor>
          <Technology name="React JS"/>
      </ExceptionInterceptor>
      <ExceptionInterceptor>
          <Technology name="JAVA"/>
      </ExceptionInterceptor>
      <ExceptionInterceptor>
          <Technology name="BMW"/>
      </ExceptionInterceptor>
      <br/>
      <br/>
      <br/>
      <h1>Exception Handled properly</h1>
    </div>
  );
};

export default template;
