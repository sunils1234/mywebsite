import "./ExceptionInterceptor.css";
import React from "react";

function template() {
  return (
    <div className="exception-interceptor">
      {this.state.isException ?<h3>May be an invalid technology</h3> : this.props.children}
    </div>
  );
};

export default template;
