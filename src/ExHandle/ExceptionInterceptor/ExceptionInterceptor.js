import React    from "react";
import template from "./ExceptionInterceptor.jsx";

class ExceptionInterceptor extends React.Component {
  constructor(){
    super();
     console.log('ExceptionInterceptor constructor invoked');
    this.state={
       isException:false
    }
  }
  static getDerivedStateFromError(){
    console.log('ExceptionInterceptor getDerivedStateFromError invoked');
    return {
      isException:true
    }
  }
  render() {
    return template.call(this);
  }
}

export default ExceptionInterceptor;
