import React    from "react";
import template from "./Quiz.jsx";

class Quiz extends React.Component {
  constructor(){
    super();
    this.state={
      questionares:[],
      timeleft:20,
      ansObj:{},
      marks:0,
      isSubmitted:false
    }
    this.optChange=this.optChange.bind(this);
    this.submitTheTest=this.submitTheTest.bind(this);
  }
  optChange(evnt){
     // debugger;
     // let val = evnt.target.value;
     // let key= evnt.target.name;
      const{name,value,type,checked} =evnt.target;
      if(type=='checkbox'){
         let ansopted= this.state.ansObj[name]||'';
            if(checked){
               ansopted= ansopted+value;
            }else{
               ansopted=ansopted.replace(value,'');
            }
            if(ansopted.length>1){
              ansopted=ansopted.split("").sort().join('');
            }
            this.setState({
                ansObj:{
                  ...this.state.ansObj,
                  [name]:ansopted
                }
              });
      }else{
        this.setState({
                ansObj:{
                  ...this.state.ansObj,
                  [name]:value
                }
              });
      }
      
  }
  componentDidMount(){
     let questionaresArr=[
        {
        id:1,
        que:'An HTML document can contain____',
        opt1:'Attributes',
        opt2:'Tags',
        opt3:'Raw text',
        opt4:'None',
        ans:'ABC',
        type:'m'
        },
        {
        id:2,
        que:'A page designed in HTML is called _____',
        opt1:'Application',
        opt2:'Cover page',
        opt3:'Front-end',
        opt4:'Web Page',
        ans:'D',
        type:'s'
        },
        {
        id:3,
        que:'An HTML document is saved with the ____ extension',
        opt1:'.htl',
        opt2:'.html',
        opt3:'.hml',
        opt4:'.hmtl',
        ans:'B',
        type:'s'
        },
        {
        id:4,
        que:'An HTML document is saved with the ____ extension',
        opt1:'.htl',
        opt2:'.html',
        opt3:'.hml',
        opt4:'.hmtl',
        ans:'B',
        type:'s'
        },
        {
          id:5,
          que:'HTML stands for',
          opt1:'Hyper text markup language',
          opt2:'Higher text markup language',
          opt3:'Hidden text maker language',
          opt4:'Hyper text markup language',
          ans:'AD',
          type:'m'
        }
    ];
      this.setState({
            questionares:questionaresArr
      });
     this.timerVal= setInterval(()=>{
      this.setState({
        timeleft: this.state.timeleft-1
      })
      if(this.state.timeleft==0){
        this.submitTheTest();
      }
    },1000);
  }

  render() {
    return template.call(this);
  }
  submitTheTest(){
    clearInterval(this.timerVal);
    let totmark=0;
    Object.keys(this.state.ansObj).forEach((qn)=>{
       let defindqa=this.state.questionares.find((origqn)=>{
          return origqn.id==qn
       })
       let anskey=defindqa.ans;
       let ansgiven=this.state.ansObj[qn];
       if(anskey==ansgiven){
         totmark++;
       }
    })
      this.setState({
        marks:totmark,
        isSubmitted:true
      })
    console.log(this.state.ansObj);
  }
}

export default Quiz;
