import "./Quiz.css";
import React from "react";

function template() {
  const {questionares,timeleft,marks,isSubmitted}= this.state;
  return (
    <div className="quiz">
      <h1>Quiz</h1>
      {
        questionares.map((obj,index)=>{
          const {id,que,opt1,opt2,opt3,opt4,ans,type}=obj;
          return <div key={index}>
                   <h3>Q{id}. {que}</h3>
                   <div>{type=='s'? <input disabled={isSubmitted} onChange={this.optChange} value="A" name={id} type='radio'/>:<input name={id} disabled={isSubmitted}  value="A" onChange={this.optChange} type='checkbox'/>}{opt1}</div>
                   <div>{type=='s'? <input disabled={isSubmitted} onChange={this.optChange} value="B" name={id} type='radio'/>:<input name={id} disabled={isSubmitted}  value="B" onChange={this.optChange} type='checkbox'/>}{opt2}</div>
                   <div>{type=='s'? <input disabled={isSubmitted} onChange={this.optChange} value="C" name={id} type='radio'/>:<input name={id} disabled={isSubmitted}  value="C" onChange={this.optChange} type='checkbox'/>}{opt3}</div>
                   <div>{type=='s'? <input disabled={isSubmitted} onChange={this.optChange} value="D" name={id} type='radio'/>:<input name={id} disabled={isSubmitted}  value="D" onChange={this.optChange} type='checkbox'/>}{opt4}</div>                    
            </div>
        })
      }
      <br/>
      <h2><button disabled={isSubmitted} onClick={this.submitTheTest}>Submit</button></h2>
      <b className="timer">{timeleft}</b>
      <h2>{marks}</h2>
          <br/>
    <br/>
    <br/>
    <br/>
    <h1>Happy hours</h1>
    </div>

  );
};

export default template;
