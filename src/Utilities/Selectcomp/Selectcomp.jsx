import "./Selectcomp.css";
import React from "react";

function template() {
  //const technologies=['HTML','CSS','JAVA SCRIPT','REACT JS'];
  return (
    <div className="selectcomp">
      <h1>Selectcomp</h1>
      <ol>
        {
        this.props.details.map((v,i)=>{
           return <li key={i}>{v}</li>
        })
        }
      </ol>
    </div>
  );
};

export default template;
