import "./Higheroc.css";
import React from "react";
import ClickCount from './ClickCount/index';
import HoverCount from './HoverCount/index';

function template() {
  return (
    <div className="higheroc">
      <h1>Higheroc</h1>
      <ClickCount/>
      <HoverCount/>
    </div>
  );
};

export default template;
