import React    from "react";
import template from "./Higheroc.jsx";

class Higheroc extends React.Component {
  render() {
    return template.call(this);
  }
}

export default Higheroc;
