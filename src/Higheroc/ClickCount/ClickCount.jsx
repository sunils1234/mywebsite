import "./ClickCount.css";
import React from "react";

function template() {
  return (
    <div className="click-count">
      <h1>ClickCount</h1>
      <button onClick={this.props.operationCount}>Click count:{this.props.count}</button>
    </div>
  );
};

export default template;
