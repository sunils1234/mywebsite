import "./Services.css";
import React from "react";
import Selectcomp from '../Utilities/Selectcomp/index';

function template() {
  return (
    <div className="services">
      <h1>List of Services our organization provides</h1>
      <Selectcomp details={this.services}/>
    </div>
  );
};

export default template;
