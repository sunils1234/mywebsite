import React    from "react";
import template from "./Services.jsx";

class Services extends React.Component {
  constructor(){
    super();
    this.services=['Software Development','ML & AI','SEO','SolutionsNServices','Cloud Services','Hardware Infrastructure','Networking Solutions'];
  }
  render() {
    return template.call(this);
  }
}

export default Services;
