import "./Menu.css";
import React,{lazy,Suspense} from "react";
//import Home from '../Home/index';
//import About from '../About/index';
//import Services from '../Services/index';
//import ContactUs from '../ContactUs/index';
//import Lists from '../Lists/index';
//import Team from '../Team/index';

import {HashRouter,Route} from 'react-router-dom';

const Home=lazy(()=>import('../Home/index'));
const About=lazy(()=>import('../About/index'));
const Services=lazy(()=>import('../Services/index'));
const ContactUs=lazy(()=>import('../ContactUs/index'));
const Lists=lazy(()=>import('../Lists/index'));
const Team=lazy(()=>import('../Team/index'));
const Higheroc=lazy(()=>import('../Higheroc/index'));
const ExHandle=lazy(()=>import('../ExHandle/index'));
const Quiz=lazy(()=>import('../Quiz/index'));

function template() {
  return (
    <div>
    <div className="menu">
      <div className="menu-items">
          <a href="#/home">Home</a>
          <a href="#/about">About</a>
          <a href="#/services">Services</a>
          <a href="#/contactus">Contact Us</a>
          <a href='#/lists'>Lists</a>
          <a href='#/teams'>Teams</a>
          <a href='#/higheroc'>higheroc</a>
          <a href='#/ExHandle'>ExHandle</a>
          <a href='#/quiz'>Quiz</a>
      </div>
     </div>
    <HashRouter>
      <Suspense fallback="Loading...">
           <Route path="/" exact component={Home}/>
           <Route path="/home" component={Home}/>
           <Route path="/about" component={About}/>
           <Route path="/services" component={Services}/>
           <Route path="/contactus" component={ContactUs}/>
           <Route path="/lists" component={Lists}/>
           <Route path="/teams" component={Team}/>
           <Route path="/higheroc" component={Higheroc}/>
           <Route path="/ExHandle" component={ExHandle}/>
           <Route path="/quiz" component={Quiz}/>
          </Suspense>
       </HashRouter>
    </div>
  );
};

export default template;
